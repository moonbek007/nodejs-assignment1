import express from "express";
import fs from "fs";

const app = express();
const PORT = 8080;
const SERVER_ERROR = { message: "Server Error" };

app.use(express.json());
app.use((req, res, next) => {
  console.log(req.method, req.originalUrl);
  next();
});

app.get("/", (req, res) => {
  res
    .status(200)
    .send("<h1 style='text-align:center'>Node.js Assignment 1</h1>");
});

app.post("/api/files", (req, res) => {
  const { filename, content } = req.body;
  if (!filename) {
    res.status(400).json({ message: "Please specify 'filename' parameter" });
  } else if (!content) {
    res.status(400).json({ message: "Please specify 'content' parameter" });
  } else if (!hasValidExtension(filename)) {
    res.status(400).json({ message: "Invalid file extension" });
  } else {
    fs.mkdir("./uploaded", { recursive: true }, (err) => {
      if (err) {
        console.log(err);
      }
    });
    fs.writeFile(`./uploaded/${filename}`, content, "utf-8", (err) => {
      if (err) {
        res.status(500).json(SERVER_ERROR);
      } else {
        res.json({ message: "File successfully created" });
      }
    });
  }
});

app.get("/api/files", (req, res) => {
  fs.mkdir("./uploaded", { recursive: true }, (err) => {
    if (err) {
      console.log(err);
    }
  });
  fs.readdir("./uploaded", (err, files) => {
    if (err) {
      res.status(500).json(SERVER_ERROR);
    } else {
      res.status(200).json({
        message: "Success",
        files: [...files],
      });
    }
  });
});

app.get("/api/files/:filename", (req, res) => {
  const { filename } = req.params;
  fs.readdir("./uploaded", (err, files) => {
    if (err) {
      res.status(500).json(SERVER_ERROR);
    } else {
      if (doesFileExist(filename, files)) {
        fs.readFile(`./uploaded/${filename}`, "utf-8", (err, data) => {
          if (err) {
            res.status(500).json(SERVER_ERROR);
          } else {
            fs.stat(`uploaded/${filename}`, (err, stats) => {
              if (err) {
                res.status(500).json(SERVER_ERROR);
              } else {
                res.status(200).json({
                  message: "Success",
                  filename: filename,
                  content: data,
                  extension: filename.substring(
                    filename.search(/\.[a-z]+$/) + 1
                  ),
                  uploadedDate: stats.birthtime,
                });
              }
            });
          }
        });
      } else {
        res
          .status(400)
          .json({ message: `No file with '${filename}' filename found` });
      }
    }
  });
});

app.put("/api/files/:filename", (req, res) => {
  const { filename } = req.params;
  const { action, payload } = req.body;
  if (!action) {
    res.status(400).json({ message: "Please specify 'action' parameter" });
  } else if (!payload) {
    res.status(400).json({ message: "Please specify 'payload' parameter" });
  } else if (action === "RENAME") {
    fs.rename(`./uploaded/${filename}`, `./uploaded/${payload}`, (err) => {
      if (err) {
        res.status(500).json(SERVER_ERROR);
      } else {
        res.status(200).json({ message: "Success", filename: payload });
      }
    });
  } else if (action === "APPEND") {
    fs.appendFile(`./uploaded/${filename}`, payload, (err) => {
      if (err) {
        res.status(500).json(SERVER_ERROR);
      } else {
        fs.readFile(`./uploaded/${filename}`, "utf-8", (err, data) => {
          if (err) res.status(500).json(SERVER_ERROR);
          else {
            res.status(500).json({
              message: "Success",
              filename: filename,
              content: data,
            });
          }
        });
      }
    });
  } else {
    res.status(400).json({ message: "Client Error" });
  }
});

app.delete("/api/files/:filename", (req, res) => {
  const { filename } = req.params;
  fs.unlink(`uploaded/${filename}`, (err) => {
    if (err) {
      res.status(400).json({ message: "Client Error" });
    } else {
      res.status(200).json({
        message: `Successly deleted file with '${filename}' filename`,
      });
    }
  });
});

app.listen(PORT, () => {
  console.log(`Starting the server on port ${PORT}`);
});

const hasValidExtension = (filename) => {
  const extensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
  for (let i = 0; i < extensions.length; i++) {
    const regex = new RegExp(`${extensions[i]}$`);
    if (regex.test(filename)) {
      return true;
    }
  }
  return false;
};

const doesFileExist = (filename, files) => {
  for (let i = 0; i < files.length; i++) {
    if (files[i] === filename) {
      return true;
    }
  }
  return false;
};

//   let files = [];
//   function throughDirectory(directory) {
//     fs.readdirSync(directory).forEach((File) => {
//       const relative = path.join(directory, File);
//       if (fs.statSync(relative).isDirectory())
//         return throughDirectory(relative);
//       else return files.push(relative.split("\\").join("/"));
//     });
//   }
//   throughDirectory("./uploaded");
//   console.log(files);

// app.put("/api/files",(req,res)=>{
//     const {password} = req.query
//     if (password) {
//       fs.readFile("./passwords.json", "utf-8", (err, data) => {
//         if (err) {
//           fs.writeFile(
//             "./passwords.json",
//             `{ "${filename}" : "${password}" }`,
//             (err) => {
//               if (err) console.log(err);
//             }
//           );
//         } else {
//           const passwords = JSON.parse(data);
//           passwords[filename] = password;
//           fs.writeFile("./passwords.json", JSON.stringify(passwords), (err) => {
//             if (err) console.log(err);
//           });
//         }
//       });
//     }
// })
